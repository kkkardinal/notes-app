import { createApp } from 'vue'
import App from './pages/App.vue'
import './css/app.scss';
import './css/index.scss';

createApp(App).mount('#app');
