import { Note } from '@/abstracts/note';
import { computed, reactive } from 'vue';
import NoteModel from '../models/notes/note-model';
import { useFolderStore } from './use-folder-store';
import { NoteState } from '../abstracts/store/note';

const noteState = reactive<NoteState>({
    notes: [],
    noteText: '',
    activeNoteId: null
})

const { activeFolderId } = useFolderStore();

export function useNoteStore() {
    const setNotes = (notes: Note[]) => {
        noteState.notes = notes;
    }

    const setActiveNoteId = (id: number | null) => {
        noteState.activeNoteId = id;
    }

    const setNoteText = (noteText: string) => {
        noteState.noteText = noteText;
    }

    const notes = computed(() => noteState.notes);
    const activeNoteId = computed(() => noteState.activeNoteId)
    const noteText = computed(() => noteState.noteText)
    const activeFolderNotes = computed(() => NoteModel.getActiveFolderNotes(noteState.notes, activeFolderId.value));

    const loadNotes = async () => {
        const notes = await NoteModel.getNotes();

        setNotes(notes);
    }

    const getActiveFolderNotes = async(id: number) => {
        const notes = NoteModel.getActiveFolderNotes(noteState.notes, id);

        setNotes(notes);
    }

    const deleteNote = async (id: number) => {
        const notes = await NoteModel.removeNote(id, noteState.notes);

        setNotes(notes);
    }

    const addNote = async (note: Note) => {
        const notes = await NoteModel.addNote(note, noteState.notes);

        setNotes(notes);
    }

    const updateNote = async (note: Note) => {
        const notes = await NoteModel.updateNote(note, noteState.notes);

        setNotes(notes);
    }

    return {
        setNotes,
        setActiveNoteId,
        setNoteText,
        notes,
        activeNoteId,
        noteText,
        activeFolderNotes,
        loadNotes,
        getActiveFolderNotes,
        deleteNote,
        addNote,
        updateNote
    }
}