import { Folder } from '@/abstracts/folder';
import { computed, reactive } from 'vue';
import FolderModel from '../models/folders/folder-model';
import { FolderState } from '../abstracts/store/folder';

const folderState = reactive<FolderState>({
    folders: [],
    activeFolderId: null
})

export function useFolderStore() {
    const setFolders = (folders: Folder[]) => {
        folderState.folders = folders;
    }

    const setActiveFolderId = (id: number) => {
        folderState.activeFolderId = id;
    }

    const folders = computed(() => folderState.folders);

    const activeFolderId = computed(() => folderState.activeFolderId);

    const loadFolders = async () => {
        const folders = await FolderModel.getFolders();

        setFolders(folders)
    }

    const addFolder = async (folder: Folder) => {
        const folders = await FolderModel.addFolder(folder, folderState.folders);

        setFolders(folders);
    }

    const updateFolder = async (folder: Folder) => {
        const folders = await FolderModel.updateFolder(folder, folderState.folders);

        setFolders(folders);
    }

    const deleteFolder = async ( id: number) => {
        const folders = await FolderModel.removeFolder(id, folderState.folders);

        setFolders(folders);
    }

    return {
        setFolders,
        setActiveFolderId,
        folders,
        activeFolderId,
        loadFolders,
        deleteFolder,
        addFolder,
        updateFolder
    }
}