import moment from 'moment';

export default class Date {
    public static getDate(format: string = 'L'): string {
        return moment().format(format);
    }
}