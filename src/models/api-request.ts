import { ApiMethod } from '../abstracts/api-method';
import { stringify } from 'flatted';

export default class ApiRequest {
    protected static headers = { 'Content-Type': 'application/json' };

    public static async makeGetRequest(url: string) {
        const data = await fetch(url);

        return data.json();
    }

    public static async makePostRequest<T>(url: string, body: T) {
        await fetch(url, {
            method: ApiMethod.POST,
            headers: this.headers,
            body: this.formBody(body)
        })
    }

    public static async makePutRequest<T extends { id: number }>(url: string,  body: T) {
        await fetch(url + `${body.id}`, {
            method: ApiMethod.PUT,
            headers: this.headers,
            body: this.formBody(body)
        })
    }

    public static async makeDeleteRequest(url: string, id: number) {
        await fetch(url + `${id}`, {
            method: ApiMethod.DELETE,
        })
    }

    public static formBody<T>(body: T) {
        return stringify(body)
    }
}