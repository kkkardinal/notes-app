import NoteApiRequest from './note-api-request';
import { Note } from '../../abstracts/note';

export default class NoteModel {
    private static shortNoteLength = 35;

    public static async getNotes(): Promise<Note[]> {
        return NoteApiRequest.getNotes();
    }

    public static getNotesCount(notes: Note[], id: number): number {
        const filteredNotes = this.getActiveFolderNotes(notes, id)

        return filteredNotes ? filteredNotes.length : 0;
    }

    public static getActiveFolderNotes(notes: Note[], id: number | null): Note[] {
        if (!id) {
            return [];
        }

        return notes.filter(note => note.folderId === id);
    }

    public static async addNote(body: Note, notes: Note[]): Promise<Note[]> {
        await NoteApiRequest.addNote(body);

        return this.addNoteInList(body, notes);
    }

    public static addNoteInList(body: Note, notes: Note[]): Note[] {
        notes.push(body);

        return notes;
    }

    public static async removeNote(id: number, notes: Note[]): Promise<Note[]> {
        await NoteApiRequest.deleteNote(id);

        return this.removeNoteFromList(id, notes);
    }

    public static removeNoteFromList(id: number, notes: Note[]): Note[] {
        return notes.filter(note => note.id != id);
    }

    public static async updateNote(body: Note, notes: Note[]): Promise<Note[]> {
        await NoteApiRequest.updateNote(body);

        return this.changeNoteList(body, notes);
    }

    public static changeNoteList(body: Note, notes: Note[]): Note[] {
        const index = this.getIndexById(notes, body.id);

        notes[index] = body;

        return notes;
    }

    public static getIndexById(notes: Note[], id: number): number {
        return notes.findIndex(note => note.id === id);
    }

    public static shortenNote(note: string) {
        return note.substring(0, this.shortNoteLength);
    }
}