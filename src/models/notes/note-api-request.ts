import ApiRequest from '../api-request';

export default class NoteApiRequest extends ApiRequest {
    private static url: string = 'http://localhost:3016/notes/';

    public static async getNotes() {
        return this.makeGetRequest(this.url);
    }

    public static async addNote(body: any): Promise<void> {
        await this.makePostRequest(this.url, body);
    }

    public static async updateNote(body: any): Promise<void> {
        await this.makePutRequest(this.url, body)
    }

    public static async deleteNote(id: number): Promise<void> {
        await this.makeDeleteRequest(this.url, id)
    }
}