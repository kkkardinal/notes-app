import { SelectorValue, SelectorType } from '../../abstracts/common';

export const folderMenuSelectors: SelectorValue[] = [
    {
        value: SelectorType.update,
        text: 'update'
    },
    {
        value: SelectorType.delete,
        text: 'delete'
    },
];