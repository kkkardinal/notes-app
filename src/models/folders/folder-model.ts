import FolderApiRequest from './folder-api-request';
import { Folder } from '../../abstracts/folder';
import { Note } from '../../abstracts/note';

export default class FolderModel {
    public static async getFolders(): Promise<Folder[]> {
        return FolderApiRequest.getFolders();
    }

    public static async addFolder(body: Folder, folders: Folder[]): Promise<Folder[]> {
        await FolderApiRequest.addFolder(body);

        return this.addFolderInList(body, folders);
    }

    public static addFolderInList(body: Folder, folders: Folder[]): Folder[] {
        folders.push(body);

        return folders;
    }

    public static async removeFolder(id: number, folders: Folder[]): Promise<Folder[]> {
        await FolderApiRequest.deleteFolder(id);

        return this.removeFolderFromList(id, folders);
    }

    public static removeFolderFromList(id: number, folders: Folder[]): Folder[] {
        return folders.filter(folder => folder.id != id);
    }

    public static async updateFolder(body: Folder, folders: Folder[]): Promise<Folder[]> {
        await FolderApiRequest.updateFolder(body);

        return this.changeFolderList(body, folders);
    }

    public static changeFolderList(body: Folder, folders: Folder[]): Folder[] {
        const index = this.getIndexById(folders, body.id);

        folders[index] = body;

        return folders;
    }

    public static getIndexById(folders: Folder[], id: number): number {
        return folders.findIndex(folder => folder.id === id);
    }

    public static getNoteById(notes: Note[], noteId: number): Note[] {
        return notes.filter(note => note.id === noteId);
    }
}