import ApiRequest from '../api-request';

export default class FolderApiRequest extends ApiRequest {
    private static url: string = 'http://localhost:3016/folders/';

    public static async getFolders() {
        return this.makeGetRequest(this.url);
    }

    public static async addFolder(body: any): Promise<void> {
        await this.makePostRequest(this.url, body);
    }

    public static async updateFolder(body: any): Promise<void> {
        await this.makePutRequest(this.url, body)
    }

    public static async deleteFolder(id: number): Promise<void> {
        await this.makeDeleteRequest(this.url, id)
    }
}