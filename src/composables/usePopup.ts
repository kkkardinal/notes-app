import { ref } from 'vue';

export default function usePopup() {
    const isShown = ref<boolean>(false);

    const show = () => isShown.value = true;

    const hide = () => isShown.value = false;

    const toggle = () => isShown.value = !isShown;

    return {
        isShown,
        show,
        hide,
        toggle
    }
}