import { Note } from '../note';

export interface NoteState {
    notes: Note[],
    noteText: string;
    activeNoteId: number | null
}