import { Folder } from '../folder';

export interface FolderState {
    folders: Folder[],
    activeFolderId: number | null,
}