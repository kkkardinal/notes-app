export interface SelectorValue {
    value: string,
    text: string
}

export const enum SelectorType {
    update = 'update',
    delete = 'delete'
}